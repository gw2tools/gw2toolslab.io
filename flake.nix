{
  description = "gw2tools";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
    flake-utils.url = "github:numtide/flake-utils";
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    pre-commit-hooks,
  }:
    flake-utils.lib.eachDefaultSystem
    (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};
      in {
        packages.gw2tools = pkgs.mkYarnPackage rec {
          src = ./.;
          packageJSON = ./package.json;
          yarnLock = ./yarn.lock;

          buildPhase = ''
            cp ${packageJSON} ./package.json
            yarn build --outDir=$out
          '';

          dontInstall = true;
          dontFixup = true;

          # yarn2nix forces its own dist phase, so we can't just set doDist = false
          distPhase = "# do nothing";
        };

        devShells.default = pkgs.mkShell {
          inputsFrom = builtins.attrValues self.packages.${system};
          packages = [
            pkgs.python311
            pkgs.python311Packages.httpx
          ];
          inherit (self.checks.${system}.pre-commit-check) shellHook;
        };

        checks.pre-commit-check = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            alejandra.enable = true;
            deadnix.enable = true;
            statix.enable = true;

            eslint.enable = true;

            black.enable = true;
            isort.enable = true;
            ruff.enable = true;
            pyright.enable = true;
          };
          settings.eslint.extensions = "(\\.js|\\.vue)$";
        };
      }
    );
}
